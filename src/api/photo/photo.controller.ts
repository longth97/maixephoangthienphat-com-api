import { PaginationDto } from './../../core/dto/pagination.dto';
import {
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Query,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import * as path from 'path';
import { Photo } from './photo.entity';
import { PhotoService } from './photo.service';
import { PaginatedPhotosResultDto } from './dto/paginated-photo.dto';
import { AuthGuard } from '@nestjs/passport';
import { FilesInterceptor } from '@nestjs/platform-express/multer';
import { diskStorage } from 'multer';
import { existsSync, mkdirSync } from 'fs';
import { extname } from 'path';
import { randStr } from '../../core/helpers/file';

@Controller('photos')
export class PhotoController {
  constructor(private photoService: PhotoService) {}

  @UseGuards(AuthGuard())
  @Post()
  @UseInterceptors(
    FilesInterceptor('images', 10, {
      // Enable file size limits
      limits: {
        fileSize: +50000000000000000,
      },
      // Check the mimetypes to allow for upload
      fileFilter: (
        req: Request,
        file: any,
        cb: (result, isValid) => void,
      ): void => {
        if (file.mimetype.match(/(jpg|jpeg|png|gif)$/)) {
          // Allow storage of file
          cb(null, true);
        } else {
          // Reject file
          cb(
            new HttpException(
              {
                status: false,
                status_code: 404,
                message: `Định dạng file không được hỗ trợ ${extname(
                  file.originalname,
                )}`,
              },
              HttpStatus.BAD_REQUEST,
            ),
            false,
          );
        }
      },
      // Storage properties
      storage: diskStorage({
        destination: (req, file, cb) => {
          const uploadPath = 'tmp';
          // Create folder if doesn't exist
          if (!existsSync(uploadPath)) {
            mkdirSync(uploadPath);
          }
          cb(null, uploadPath);
        },
        filename: (req, file, cb) => {
          // Calling the callback passing the random name generated with the original extension name
          const timestamp = Date.now();
          const baseExt = extname(file.originalname);
          const baseName = path.basename(file.originalname, baseExt);
          // const newName = baseName.toLowerCase().replace(/(\ |-|_)+/g, "") + `${randStr(4)}${timestamp}`;
          const newName =
            baseName.toLowerCase().replace(/([ \-_])+/g, '') +
            `${randStr(4)}${timestamp}`;
          cb(null, `${newName}${baseExt}`);
        },
      }),
    }),
  )

  @UseGuards(AuthGuard())
  uploadImage(@UploadedFiles() images) {
    return this.photoService.waterMark(images.map((e) => path.join(e.path)));
  }

  @Get()
  getPhotos(
    @Query() paginationDto: PaginationDto,
  ): Promise<PaginatedPhotosResultDto> {
    paginationDto.page = Number(paginationDto.page);
    paginationDto.limit = Number(paginationDto.limit);

    return this.photoService.getPhotos({
      ...paginationDto,
      limit: paginationDto.limit,
    });
  }

  @Get('/:id')
  async getPhotoById(@Param('id') id: string): Promise<Photo> {
    return this.photoService.getPhotoById(id);
  }

  @UseGuards(AuthGuard())
  @Delete('/:id')
  async deletePhoto(@Param('id') id: string): Promise<void> {
    return this.photoService.deletePhoto(id);
  }
}
