import { IsBoolean, IsString } from "class-validator";

export class UpdateSocialDto {
  @IsString({ message: "Tên mạng xã hội phải là chuỗi" })
  name: string;

  @IsString({ message: "liên kết phải là chuỗi" })
  link?: string;

  @IsBoolean({ message: "isShow phải là kiểu boolean" })
  isShow?: boolean;
}
