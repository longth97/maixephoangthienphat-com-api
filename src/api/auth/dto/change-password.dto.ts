import { IsNotEmpty, IsString, Matches, MaxLength, MinLength } from "class-validator";

export class ChangePasswordDto {
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  @IsNotEmpty({
    message: "Không được rỗng"
  })
  oldPassword: string;

  @IsString()
  @MinLength(8)
  @MaxLength(20)
  @IsNotEmpty({
    message: "Mật khẩu Không được rỗng"
  })
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: "Mật khẩu yếu"
  })
  newPassword: string;
}
