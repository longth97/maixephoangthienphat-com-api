import { ConflictException, Injectable, InternalServerErrorException, NotFoundException } from "@nestjs/common";
import { CreatePortfolioDto } from "./dto/create-portfolio.dto";
import { Portfolio } from "./portfolio.entity";
import { PortfolioRepository } from "./portfolio.repository";
import { PaginationDto } from "../../core/dto/pagination.dto";
import { PaginatedPortfolioResultDto } from "./dto/paginated-portfolio.dto";

@Injectable()
export class PortfolioService {
  constructor(private portfolioRepository: PortfolioRepository) {
  }

  async createPortfolio(createPortfolioDto: CreatePortfolioDto): Promise<Portfolio> {
    const portfolio = new Portfolio(createPortfolioDto.title, createPortfolioDto.description, createPortfolioDto.images, createPortfolioDto.content, createPortfolioDto.link);
    try {
      await portfolio.save();
    } catch (error) {
      if (error.code === "23505") {
        throw new ConflictException("Sản phẩm đã tồn tại");
      } else {
        throw new InternalServerErrorException();
      }
    }
    return portfolio;
  }

  async getPortfolios(
    paginationDto: PaginationDto
  ): Promise<PaginatedPortfolioResultDto> {
    const skippedItems = (paginationDto.page - 1) * paginationDto.limit;
    const totalCount = await this.portfolioRepository.count();
    let isLastPage = paginationDto.page * paginationDto.limit >= totalCount;

    const { search } = paginationDto;

    let portfolios: Portfolio[];

    if (search) {
      portfolios = await this.portfolioRepository
        .createQueryBuilder("portfolio")
        .andWhere("(portfolio.title LIKE :search)", {
          search: `%${search}%`
        })
        .orderBy("title", "DESC")
        .offset(skippedItems)
        .limit(paginationDto.limit)
        .getMany();
    } else {
      portfolios = await this.portfolioRepository
        .createQueryBuilder()
        .orderBy("title", "DESC")
        .offset(skippedItems)
        .limit(paginationDto.limit)
        .getMany();
    }

    return {
      totalCount,
      page: paginationDto.page,
      limit: paginationDto.limit,
      isLastPage,
      data: portfolios
    };
  }

  async getPortfolioById(id: number): Promise<Portfolio> {
    const found = await this.portfolioRepository.findOne(id);

    if (!found) {
      throw new NotFoundException(`Không tìm thấy dự án này`);
    }

    return found;
  }

  async updatePortfolio(
    id: number,
    title?: string,
    description?: string,
    images?: string[],
    content?: string,
    link?: string
  ): Promise<Portfolio> {
    const portfolio = await this.getPortfolioById(id);

    if (title) {
      portfolio.title = title;
    }

    if (description) {
      portfolio.description = description;
    }
    if (images) {
      portfolio.images = images;
    }
    if (content) {
      portfolio.content = content;
    }

    if (link) {
      portfolio.link = link;
    }
    return this.portfolioRepository.updatePortfolio(portfolio);
  }

  async deletePortfolio(id: number): Promise<void> {
    const result = await this.portfolioRepository.delete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Không tìm thấy dự án này`);
    }
  }
}
