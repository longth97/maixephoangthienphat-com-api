import { Repository, EntityRepository } from "typeorm";
import { Portfolio } from "./portfolio.entity";
import { CreatePortfolioDto } from "./dto/create-portfolio.dto";

import {
  ConflictException,
  InternalServerErrorException
} from "@nestjs/common";

@EntityRepository(Portfolio)
export class PortfolioRepository extends Repository<Portfolio> {
  async updatePortfolio(portfolio: Portfolio): Promise<Portfolio> {
    try {
      await portfolio.save();
    } catch (error) {
      throw new InternalServerErrorException();
    }
    return portfolio;
  }
}
