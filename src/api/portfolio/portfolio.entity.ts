import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Unique
} from "typeorm";

@Entity()
@Unique(["id"])
export class Portfolio extends BaseEntity {
  constructor(
    title: string,
    description: string,
    images: string[],
    content: string,
    link: string
  ) {
    super();
    this.title = title;
    this.description = description;
    this.images = images;
    this.content = content;
    this.link = link;
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column("text", { array: true })
  images: string[];

  @Column()
  description: string;

  @Column()
  content: string;

  @Column()
  link: string;
}
