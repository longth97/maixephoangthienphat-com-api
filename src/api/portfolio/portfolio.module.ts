import { Module } from '@nestjs/common';
import { PortfolioController } from './portfolio.controller';
import { PortfolioService } from './portfolio.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PortfolioRepository } from './portfolio.repository';
import { AuthModule } from '../auth/auth.module';

@Module({
  controllers: [PortfolioController],
  imports: [TypeOrmModule.forFeature([PortfolioRepository]), AuthModule],
  providers: [PortfolioService],
})
export class PortfolioModule {}
