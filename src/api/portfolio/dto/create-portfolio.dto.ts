import { IsNotEmpty, IsString } from "class-validator";

export class CreatePortfolioDto {
  @IsNotEmpty({ message: "Tên dự án không được để trốn" })
  @IsString()
  title: string;

  @IsNotEmpty({ message: "Mô tả dự án không được để trốn" })
  @IsString()
  description: string;

  @IsNotEmpty({ message: "Hình ảnh không được để trống" })
  images: string[];

  @IsNotEmpty({ message: "Nội dung dự án không được để trốn" })
  @IsString()
  content: string;

  @IsNotEmpty({ message: "Link sản phẩm không được để trốn" })
  @IsString()
  link: string;
}
