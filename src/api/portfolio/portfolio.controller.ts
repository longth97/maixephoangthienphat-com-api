import {
  Body,
  Controller,
  Delete,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseGuards,
  ValidationPipe
} from "@nestjs/common";
import { PortfolioService } from "./portfolio.service";
import { AuthGuard } from "@nestjs/passport";
import { Get } from "@nestjs/common";
import { PaginationDto } from "../../core/dto/pagination.dto";
import { PaginatedPortfolioResultDto } from "./dto/paginated-portfolio.dto";
import { CreatePortfolioDto } from "./dto/create-portfolio.dto";
import { Portfolio } from "./portfolio.entity";

@Controller("portfolios")
export class PortfolioController {
  constructor(private portfolioService: PortfolioService) {
  }

  @UseGuards(AuthGuard())
  @Post()
  createPortfolio(
    @Body(ValidationPipe) createPortfolioDto: CreatePortfolioDto
  ): Promise<Portfolio> {
    return this.portfolioService.createPortfolio(createPortfolioDto);
  }

  @Get()
  getPortfolios(
    @Query(ValidationPipe) paginationDto: PaginationDto
  ): Promise<PaginatedPortfolioResultDto> {
    paginationDto.page = Number(paginationDto.page);
    paginationDto.limit = Number(paginationDto.limit);

    return this.portfolioService.getPortfolios({
      ...paginationDto,
      limit: paginationDto.limit
    });
  }

  @Get("/:id")
  getPortfolioById(@Param("id", ParseIntPipe) id: number): Promise<Portfolio> {
    return this.portfolioService.getPortfolioById(id);
  }

  @UseGuards(AuthGuard())
  @Patch("/:id")
  updatePortfolio(
    @Param("id", ParseIntPipe) id: number,
    @Body(ValidationPipe)
      portfolio: {
      title: string;
      description: string;
      images: string[];
      content: string;
      link: string;
    }
  ): Promise<Portfolio> {
    return this.portfolioService.updatePortfolio(
      id,
      portfolio.title,
      portfolio.description,
      portfolio.images,
      portfolio.content,
      portfolio.link
    );
  }

  @UseGuards(AuthGuard())
  @Delete("/:id")
  deletePortfolio(@Param("id", ParseIntPipe) id: number): Promise<void> {
    return this.portfolioService.deletePortfolio(id);
  }
}
