import { Controller, Get, Param, Query, ValidationPipe } from "@nestjs/common";
import { ViewsService } from "./views.service";
import { PaginationDto } from "../../core/dto/pagination.dto";

@Controller("views")
export class ViewsController {
  constructor(private readonly service: ViewsService) {
  }

  @Get()
  async getViews(@Query(ValidationPipe) count: number): Promise<number> {
    return await this.service.findAll(count);
  }
}
