import { Injectable, InternalServerErrorException } from "@nestjs/common";
import { Views } from "./views.entity";
import { ViewsRepository } from "./views.repository";

@Injectable()
export class ViewsService {
  constructor(private readonly repository: ViewsRepository) {
  }

  async createViews(view: number): Promise<void> {
    const views = new Views();
    views.views = view;
    try {
      await views.save();
    } catch (err) {
      console.log(err);
      throw new InternalServerErrorException({ err });
    }
  }

  async findAll(count: number): Promise<number> {
    let views: number;

    try {
      if (Number(count["count"]) === 1) {

        views = await this.repository.createQueryBuilder("views")
          .where("DATE(views.createAt) = CURRENT_DATE")
          .getCount();

        console.log(`get views in day ${views}`);

      } else {
        views = await this.repository.createQueryBuilder("views").getCount();
      }
    } catch (err) {
      console.log(err);
      throw new InternalServerErrorException({ err });
    }
    return views;
  }
}
