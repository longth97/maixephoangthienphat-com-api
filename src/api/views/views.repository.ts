import { EntityRepository, Repository } from "typeorm";
import { Views } from "./views.entity";

@EntityRepository(Views)
export class ViewsRepository extends Repository<Views> {

}