import { BaseEntity, Column, Entity, PrimaryGeneratedColumn, Unique } from "typeorm";

@Entity()
export class Views extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  views: number;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  createAt: string;
}